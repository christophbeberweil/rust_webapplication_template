-- Add up migration script here
CREATE TABLE IF NOT EXISTS message_of_the_day (
    id uuid NOT NULL PRIMARY KEY DEFAULT gen_random_uuid(),
    text TEXT NOT NULL,
    user_id uuid NOT NULL, 
    created_at  TIMESTAMP with time zone NOT NULL DEFAULT NOW()
);
