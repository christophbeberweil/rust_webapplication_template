use axum::{
    async_trait,
    body::Body,
    extract::State,
    extract::{FromRef, FromRequestParts},
    http::request::Parts,
    http::{header, Request, StatusCode},
    middleware::Next,
    response::IntoResponse,
    response::Response,
    Json, RequestPartsExt,
};
use axum_extra::{
    headers::{authorization::Bearer, Authorization},
    TypedHeader,
};
use jsonwebtoken::{decode, DecodingKey, Validation};
use serde::{Deserialize, Serialize};
use shared::types::{token_claims::TokenClaims, user::UserId};
use std::fmt::Display;
use uuid::Uuid;

use crate::AppState;

use serde_json::json;

#[derive(Debug, Serialize)]
pub struct ErrorResponse {
    pub status: &'static str,
    pub message: String,
}

pub async fn auth_layer(
    //cookie_jar: CookieJar,
    State(state): State<AppState>,
    mut req: Request<Body>,
    next: Next,
) -> Result<impl IntoResponse, (StatusCode, Json<ErrorResponse>)> {
    let token = {
        req.headers()
            .get(header::AUTHORIZATION)
            .and_then(|auth_header| auth_header.to_str().ok())
            .and_then(|auth_value| {
                /*
                // long, tedious solution
                if auth_value.starts_with("Bearer ") {
                    Some(auth_value[7..].to_owned())
                } else {
                    None
                }
                */

                // clippy solution :)
                auth_value
                    .strip_prefix("Bearer ")
                    .map(|token| token.to_owned())
            })
    };

    let token = token.ok_or_else(|| {
        let json_error = ErrorResponse {
            status: "fail",
            message: "You are not logged in, please provide token".to_string(),
        };
        (StatusCode::UNAUTHORIZED, Json(json_error))
    })?;

    let claims = decode::<TokenClaims>(
        &token,
        &DecodingKey::from_secret(state.config.jwt_secret.as_ref()),
        &Validation::default(),
    )
    .map_err(|e| {
        tracing::error!("Claims could not be extracted from token {token}, error: {e}");
        let json_error = ErrorResponse {
            status: "fail",
            message: format!("Invalid token {}", e.to_string()),
        };
        (StatusCode::UNAUTHORIZED, Json(json_error))
    })?
    .claims;

    let user = state
        .store
        .get_user(UserId(claims.user_id))
        .await
        .map_err(|e| {
            tracing::event!(
                tracing::Level::ERROR,
                "The user belonging to this token no longer exists. User Id: {} error: {:?}",
                claims.user_id,
                e
            );
            let json_error = ErrorResponse {
                status: "fail",
                message: "The user belonging to this token no longer exists".to_string(),
            };
            (StatusCode::UNAUTHORIZED, Json(json_error))
        })?;

    req.extensions_mut().insert(user);
    Ok(next.run(req).await)
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Claims {
    pub exp: usize,
    pub user_id: Uuid,
}

impl Display for Claims {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Exp: {}, user_id: {}", self.exp, self.user_id)
    }
}

#[derive(Debug)]
pub enum AuthError {
    InvalidToken,
}

impl IntoResponse for AuthError {
    fn into_response(self) -> Response {
        let (status, error_message) = match self {
            AuthError::InvalidToken => (StatusCode::BAD_REQUEST, "Invalid token"),
        };
        let body = Json(json!({
            "error": error_message,
        }));
        (status, body).into_response()
    }
}

#[async_trait]
impl<S> FromRequestParts<S> for Claims
where
    AppState: FromRef<S>,
    S: Send + Sync,
{
    type Rejection = AuthError;

    /// Extract the token from the authorization header

    async fn from_request_parts(parts: &mut Parts, state: &S) -> Result<Self, Self::Rejection> {
        // transform the generic state into a concrete AppState
        let state = AppState::from_ref(state);

        let TypedHeader(Authorization(bearer)) = parts
            .extract::<TypedHeader<Authorization<Bearer>>>()
            .await
            .map_err(|_| AuthError::InvalidToken)?;
        // Decode the user data
        let token_data = decode::<Claims>(
            bearer.token(),
            &DecodingKey::from_secret(state.config.jwt_secret.as_ref()),
            &Validation::default(),
        )
        .map_err(|_| AuthError::InvalidToken)?;

        Ok(token_data.claims)
    }
}
