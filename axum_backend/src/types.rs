use crate::{store::Store, config::Config};

#[derive(Clone, Debug)]
pub struct AppState {
    pub store: Store,
    pub config: Config,
}
