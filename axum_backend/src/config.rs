use chrono::Duration as ChronoDuration;

#[derive(Debug, Clone)]
pub struct Config {
    /// connection url for the POSTGRES database
    pub database_url: String,
    /// Give the users a possibility to specify their secret for the jwt session
    pub jwt_secret: String,
    pub jwt_expires_in: ChronoDuration,
    /// Port the webserver should bind to
    pub app_port: u16,
}

impl Config {
    pub fn init() -> Config {
        let database_url = std::env::var("DATABASE_URL").expect("DATABASE_URL must be set");
        let jwt_secret = std::env::var("JWT_SECRET").expect("JWT_SECRET must be set");
        let jwt_expires_in = ChronoDuration::hours(
            std::env::var("JWT_EXPIRES_IN_HOURS")
                .expect("JWT_EXPIRES_IN_HOURS must be set")
                .parse::<i64>()
                .expect("that this string for JWT_EXPIRES_IN_HOURS is parseable as i64"),
        );

        let app_port = std::env::var("PORT").unwrap_or("3000".to_owned());

        Config {
            database_url,
            jwt_secret,
            jwt_expires_in,
            app_port: app_port.parse::<u16>().unwrap_or(3000),
        }
    }
}
