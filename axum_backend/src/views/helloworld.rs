use axum::{response::IntoResponse, Json};
use serde::Serialize;

#[derive(Serialize)]
struct Hello {
    name: String,
}

pub async fn helloworld() -> impl IntoResponse {
    let hello = Hello {
        name: String::from("world"),
    };
    Json(hello)
}


