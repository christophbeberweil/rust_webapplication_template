use std::sync::Arc;

use axum::{extract::State, http::StatusCode, response::IntoResponse, Extension, Json};
use shared::types::{api_messages::ErrorApiResponse, user::User};

use crate::types::AppState;

pub async fn delete_own_useraccount(
    State(state): State<Arc<AppState>>,
    Extension(user): Extension<User>,
) -> impl IntoResponse {
    let Ok(_) = state.store.delete_user(user).await else {
        return (
            StatusCode::INTERNAL_SERVER_ERROR,
            Json(ErrorApiResponse {
                error: "could not delete the user".to_string(),
            }),
        )
            .into_response();
    };

    StatusCode::OK.into_response()
}
