use std::sync::Arc;

use axum::{extract::State, response::IntoResponse, Json};
use http::StatusCode;
use shared::types::api_messages::ErrorApiResponse;

use crate::types::AppState;

pub async fn list_messages_of_the_day(
    State(state): State<Arc<AppState>>,
) -> Result<impl IntoResponse, (StatusCode, Json<serde_json::Value>)> {
    match state.store.list_messages_of_the_day().await {
        Ok(messages) => Ok(Json(messages).into_response()),
        Err(e) => {
            //asd
            tracing::event!(
                tracing::Level::ERROR,
                "list_messages_of_the_day, error: {}",
                e.to_string()
            );
            Ok((
                StatusCode::BAD_REQUEST,
                Json(ErrorApiResponse {
                    error: "could not list the messages of the day".to_string(),
                }),
            )
                .into_response())
        }
    }
}
