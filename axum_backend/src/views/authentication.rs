use std::sync::Arc;

use argon2::{Argon2, PasswordHash, PasswordVerifier};
use axum::{
    extract::State,
    http::{Response, StatusCode},
    response::IntoResponse,
    Json,
};
use serde_json::json;
use shared::types::{
    error_response::ErrorResponse,
    token_claims::TokenClaims,
    user::{NewUser, UserId, UserWithPasswordHash},
};
use sqlx::{postgres::PgRow, Row};

use crate::types::AppState;

pub async fn login(
    State(state): State<Arc<AppState>>,
    Json(body): Json<NewUser>,
) -> Result<impl IntoResponse, (StatusCode, Json<serde_json::Value>)> {
    tracing::event!(tracing::Level::WARN, "login_user_handler_api");
    println!("login user handler api");

    let user = sqlx::query("SELECT * FROM users WHERE username = $1")
        .bind(body.username.to_string())
        .map(|row: PgRow| UserWithPasswordHash {
            id: UserId(row.get("id")),
            username: row.get("username"),
            password_hash: row.get("password_hash"),
        })
        .fetch_optional(&state.store.connection)
        .await
        .map_err(|e| {
            let error_response = serde_json::json!({
                "errors": vec!(format!("Database error: {}", e)),
            });
            (StatusCode::INTERNAL_SERVER_ERROR, Json(error_response))
        })?
        .ok_or_else(|| {
            let error_response = serde_json::json!({
                "errors": vec!("Invalid username or password"),
            });
            (StatusCode::FORBIDDEN, Json(error_response))
        })?;
    tracing::event!(
        tracing::Level::INFO,
        "login_user_handler_api user: {:?}",
        user.clone()
    );

    let is_valid = match PasswordHash::new(&user.password_hash) {
        Ok(parsed_hash) => Argon2::default()
            .verify_password(body.cleartext_password.as_bytes(), &parsed_hash)
            .map_or(false, |_| true),
        Err(_) => false,
    };

    if !is_valid {
        let error_response = serde_json::json!({
            "status": "fail",
            "error": "Invalid username or password"
        });
        return Err((StatusCode::FORBIDDEN, Json(error_response)));
    }

    let now = chrono::Utc::now();
    let iat = now.timestamp() as usize;
    let exp = (now + state.config.jwt_expires_in).timestamp() as usize;
    let claims: TokenClaims = TokenClaims {
        user_id: user.id.0,
        exp,
        iat,
    };

    let token = jsonwebtoken::encode(
        &jsonwebtoken::Header::default(),
        &claims,
        &jsonwebtoken::EncodingKey::from_secret(state.config.jwt_secret.as_ref()),
    )
    .unwrap();

    let response = Response::new(json!({"status": "success", "token": token}).to_string());

    Ok(response)
}

pub async fn signup(
    State(state): State<Arc<AppState>>,
    Json(new_user): Json<NewUser>,
) -> impl IntoResponse {
    match state.store.create_user(new_user).await {
        Ok(user) => {
            tracing::event!(tracing::Level::INFO, "Saved new user {:?}", user);
            Ok((StatusCode::OK, Json(user)))
        }
        Err(e) => {
            tracing::event!(tracing::Level::ERROR, "Error when saving new user {:?}", e);
            Err((
                StatusCode::BAD_REQUEST,
                Json(ErrorResponse {
                    errors: vec![
                        "Could not create the user, maybe try another username?".to_owned()
                    ],
                }),
            ))
        }
    }
}
