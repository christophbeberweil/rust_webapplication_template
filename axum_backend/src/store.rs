use shared::authentication::password_hash::hash_password;
use shared::types::message_of_the_day::{MessageOfTheDay, MessageOfTheDayId};
use sqlx::postgres::{PgPool, PgPoolOptions, PgRow};
use sqlx::Row;

use shared::types::user::{NewUser, User, UserId, UserWithPasswordHash};

#[derive(Debug, Clone)]
pub struct Store {
    pub connection: PgPool,
}

impl Store {
    pub async fn new(db_url: &str) -> Self {
        let db_pool = match PgPoolOptions::new()
            .max_connections(5)
            .connect(db_url)
            .await
        {
            Ok(pool) => pool,
            Err(e) => panic!("Couldn't establish DB connection: {}", e),
        };
        Store {
            connection: db_pool,
        }
    }

    pub async fn create_user(&self, new_user: NewUser) -> Result<User, sqlx::Error> {
        let Ok(hashed_password) = hash_password(new_user.cleartext_password) else {
            panic!("could not hash password")
        };
        match sqlx::query(
            "INSERT INTO users (username, password_hash) values ($1, $2)  returning *",
        )
        .bind(new_user.username)
        .bind(hashed_password)
        .map(|row: PgRow| UserWithPasswordHash {
            id: UserId(row.get("id")),
            username: row.get("username"),
            password_hash: row.get("password_hash"),
        })
        .fetch_one(&self.connection)
        .await
        {
            Ok(user) => Ok(User::from_user_with_password_hash(user)),
            Err(e) => Err(e),
        }
    }

    pub async fn get_user(&self, user_id: UserId) -> Result<User, sqlx::Error> {
        sqlx::query("select * from users where id = $1")
            .bind(user_id.0)
            .map(|row: PgRow| User {
                id: UserId(row.get("id")),
                username: row.get("username"),
            })
            .fetch_one(&self.connection)
            .await
    }

    pub async fn delete_user(&self, user: User) -> Result<bool, sqlx::Error> {
        sqlx::query("DELETE FROM users where id = $1")
            .bind(user.id.0)
            .execute(&self.connection)
            .await?;

        Ok(true)
    }

    pub async fn get_message_of_the_day(
        &self,
        motd_id: MessageOfTheDayId,
    ) -> Result<MessageOfTheDay, sqlx::Error> {
        sqlx::query("select motd.*, u.username from message_of_the_day as motd left join users as u on motd.user_id = u.id where id = $1")
            .bind(motd_id.0)
            .map(|row: PgRow| MessageOfTheDay {
                id: MessageOfTheDayId(row.get("id")),
                text: row.get("text"),
                user_id: UserId(row.get("user_id")),
                user_name: row.get("username"),
                created_at: row.get("created_at")
            })
            .fetch_one(&self.connection)
            .await
    }

    pub async fn list_messages_of_the_day(&self) -> Result<Vec<MessageOfTheDay>, sqlx::Error> {
        sqlx::query("select motd.*, u.username from message_of_the_day as motd left join users as u on motd.user_id = u.id")
        .map(|row: PgRow| MessageOfTheDay {
            id: MessageOfTheDayId(row.get("id")),
            text: row.get("text"),
            user_id: UserId(row.get("user_id")),
            user_name: row.get("username"),
            created_at: row.get("created_at")
        })
        .fetch_all(&self.connection)
        .await
    }
}
