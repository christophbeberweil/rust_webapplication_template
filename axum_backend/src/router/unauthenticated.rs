use std::sync::Arc;

use crate::types::AppState;
use crate::views::authentication::{login, signup};
use crate::views::helloworld::helloworld;
use crate::views::message_of_the_day::list_messages_of_the_day;
use axum::routing::{get, post};
use axum::Router;
/// assembles a router and returns it. These routes should be accessible unauthenticated
pub fn unauthenticated_router(app_state: Arc<AppState>) -> axum::Router {
    Router::new()
        .route("/api/tmp/message_of_the_day", get(list_messages_of_the_day))
        .route("/api/signup", post(signup))
        .route("/api/login", post(login))
        .route("/", get(helloworld))
        .with_state(app_state)
}
