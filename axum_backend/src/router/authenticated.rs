use std::sync::Arc;

use crate::types::AppState;
use crate::views::message_of_the_day::list_messages_of_the_day;
use crate::views::user::delete_own_useraccount;
use axum::routing::{get, post};
use axum::Router;
/// assembles a router and returns it. These routes should be accessible unauthenticated
pub fn authenticated_router(app_state: Arc<AppState>) -> axum::Router {
    Router::new()
        .route("/api/message_of_the_day", get(list_messages_of_the_day))
        .route("/api/delte_own_useraccount", post(delete_own_useraccount))
        .with_state(app_state)
}
