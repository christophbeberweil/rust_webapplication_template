use std::net::SocketAddr;
use std::sync::Arc;

use axum::middleware;
use axum::{
    http::{
        header::{ACCEPT, AUTHORIZATION, CONTENT_TYPE},
        Method,
    },
    Router,
};
use config::Config;
use router::unauthenticated::unauthenticated_router;
use tower::ServiceBuilder;
use tower_http::cors::{Any, CorsLayer};
use tower_http::trace::TraceLayer;
use tracing_subscriber::{layer::SubscriberExt, util::SubscriberInitExt};

use crate::jwt_auth::auth_layer;
use crate::router::authenticated::authenticated_router;
use crate::types::AppState;
mod config;
mod jwt_auth;
mod router;
mod store;
mod types;
mod views;
#[tokio::main]
async fn main() {
    // init tracing setup
    tracing_subscriber::registry()
        .with(
            tracing_subscriber::EnvFilter::try_from_default_env()
                .unwrap_or_else(|_| "example_templates=debug".into()),
        )
        .with(tracing_subscriber::fmt::layer())
        .init();
    // parse environment variables and set up db connection
    let config: Config = Config::init();

    let store = store::Store::new(config.database_url.clone().as_str()).await;

    tracing::info!("📂 Running database migration",);
    // run db migrations

    match sqlx::migrate!().run(&store.clone().connection).await {
        Ok(_) => tracing::info!("✅ Database migrations successful"),
        Err(e) => tracing::error!("❌ Error in Database migrations\n{}", e),
    };

    let state = AppState {
        store: store.clone(),
        config: config.clone(),
    };

    let cors = CorsLayer::new()
        .allow_origin(Any)
        .allow_methods([Method::GET, Method::POST, Method::PUT, Method::DELETE])
        .allow_headers([AUTHORIZATION, ACCEPT, CONTENT_TYPE]);

    let unauthenticated_router = unauthenticated_router(Arc::new(state.clone()));
    let authenticated_router = authenticated_router(Arc::new(state.clone()));

    let sb = ServiceBuilder::new().layer(cors);

    // build our application with some routes
    //let app = Router::new().merge(unauthenticated_router).layer(cors);
    // todo: cors does not work because of trait bound

    let app = Router::new()
        .merge(authenticated_router)
        .route_layer(middleware::from_fn_with_state(state.clone(), auth_layer))
        .merge(unauthenticated_router)
        .layer(sb)
        .layer(TraceLayer::new_for_http());

    // run it
    let port: u16 = config.app_port;
    let addr = SocketAddr::from(([0, 0, 0, 0], port));

    let listener = tokio::net::TcpListener::bind(addr)
        .await
        .expect("that we could bind to the given address");
    tracing::info!(
        "🚀 listening on {}",
        listener
            .local_addr()
            .expect("that we could obtain the local_addr from the listener")
    );

    axum::serve(listener, app)
        .await
        .expect("that the axum server could start");
}
