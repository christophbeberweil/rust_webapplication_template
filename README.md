# Pure Rust Webapplication Template

Backend in Axum, Frontend in Yew, shared functionality in a library crate.

## Development

In order for Rust Analyzer to work properly, it is recommended to open the yew-frontend folder in its own code editor.

This is due to the fact that it is built with target wasm32-unknown-unknown and yewdux toggles features automatically based on the detected build target.
