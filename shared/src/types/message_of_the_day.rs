use serde::{Deserialize, Serialize};
use uuid::Uuid;

use super::user::User;

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Eq, Hash)]
pub struct MessageOfTheDayId(pub Uuid);

#[derive(Deserialize, Serialize, Debug, Clone, PartialEq)]
pub struct MessageOfTheDay {
    pub id: MessageOfTheDayId,
    pub content: String,
    pub author: Option<User>,
}
