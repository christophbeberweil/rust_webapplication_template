use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize, Default)]
pub struct ErrorResponse {
    pub errors: Vec<String>,
}
