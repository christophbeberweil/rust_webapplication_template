use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub struct ErrorApiResponse {
    pub error: String,
}
