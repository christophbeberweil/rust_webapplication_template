#[cfg(feature = "backend")]
use axum_login::AuthUser;
use serde::{Deserialize, Serialize};
#[cfg(feature = "backend")]
use sqlx::FromRow;
use uuid::Uuid;

#[derive(Deserialize, Serialize, Debug, Clone, PartialEq)]
#[cfg_attr(feature = "backend", derive(FromRow))]
pub struct UserId(pub Uuid);

/// represents a user for the application without hidden fields
#[derive(Deserialize, Serialize, Debug, Clone, PartialEq)]
#[cfg_attr(feature = "backend", derive(FromRow))]
pub struct User {
    pub id: UserId,
    pub username: String,
}

impl User {
    pub fn from_user_with_password_hash(usr_w_hash: UserWithPasswordHash) -> Self {
        Self {
            id: usr_w_hash.id.clone(),
            username: usr_w_hash.username.clone(),
        }
    }
}

/// a new user with a cleartext password that has not yet been hashed
#[derive(Deserialize, Serialize)]
pub struct NewUser {
    pub username: String,
    #[serde(rename = "password")]
    pub cleartext_password: String,
}

/// represents a user with their password hash for the database
/// used for checking the password
#[derive(Deserialize, Serialize, Debug, Clone, PartialEq)]
pub struct UserWithPasswordHash {
    pub id: UserId,
    pub username: String,
    pub password_hash: String,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct NewUserFormData {
    pub username: String,
    pub password: String,
    pub password_repeat: String,
}

#[derive(Deserialize, Serialize, Debug, Clone, PartialEq)]
pub struct LoginFormData {
    pub username: String,
    pub password: String,
}

#[cfg(feature = "backend")]
impl AuthUser for UserWithPasswordHash {
    type Id = Uuid;

    fn id(&self) -> Uuid {
        self.id.0
    }

    fn session_auth_hash(&self) -> &[u8] {
        self.password_hash.as_bytes()
    }
}
