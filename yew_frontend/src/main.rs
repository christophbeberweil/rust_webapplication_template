use components::app::App;

mod components;
mod methods;
mod store;

fn main() {
    yew::Renderer::<App>::new().render();
}
