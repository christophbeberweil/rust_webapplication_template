use yew::prelude::*;


#[derive(PartialEq, Properties)]
pub struct HomePageProps {}

#[function_component]
pub fn HomePage(props: &HomePageProps) -> Html {
    let HomePageProps {} = props;
    html! {
        <div class="contents">
            <h2 class="font-bold text-xl">{"Home"}</h2>
            <p>{"Welcome home 🥳"}</p>
        </div>
    }
}