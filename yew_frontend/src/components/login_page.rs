use std::ops::Deref;

use crate::components::application_logo::ApplicationLogo;
use crate::methods::login::perform_login;
use crate::methods::signup::perform_signup;
use crate::store::LoginSignupStore;
use crate::store::RootStore;
use serde::Deserialize;
use serde::Serialize;
use wasm_bindgen::JsCast;
use web_sys::EventTarget;
use web_sys::HtmlInputElement;
use yew::prelude::*;
use yewdux::prelude::*;

#[derive(Serialize, Deserialize, Default, Clone, Debug)]
pub struct LoginData {
    pub server: String,
    pub username: String,
    //    #[serde(rename(serialize = "cleartext_password"))]
    pub password: String,
    pub repeat_password: String,
}

#[derive(PartialEq, Properties)]
pub struct LoginPageProps {}

#[function_component]
pub fn LoginPage(props: &LoginPageProps) -> Html {
    let LoginPageProps {} = props;

    let (ls_store, ls_store_dispatch) = use_store::<LoginSignupStore>();

    let toggle_is_signup = {
        Callback::from(move |_| {
            ls_store_dispatch.reduce_mut(|ls_store| ls_store.is_signup = !ls_store.is_signup)
        })
    };

    // local state for login and registering
    let root_store = Dispatch::<RootStore>::global().get();
    let url = root_store.backend_url.clone().unwrap_or("".to_owned());
    let username = root_store.username.clone().unwrap_or("".to_owned());
    let login_data_state = use_state(|| LoginData {
        server: url,
        username,
        password: "".to_owned(),
        repeat_password: "".to_owned(),
    });

    //let a = login_data_state.deref().clone();
    //log!(format!("Login data state: {:?}", a));

    // while more verbose, this works
    //callback that observes the username field and saves changes in the local state
    let server_input = Callback::from({
        let cloned_state = login_data_state.clone();
        move |e: InputEvent| {
            let target: Option<EventTarget> = e.target();
            // in order to use dyn_into, use wasm_bindgen::JsCast;
            let input = target.and_then(|t| t.dyn_into::<HtmlInputElement>().ok());
            if let Some(input) = input {
                let server = input.value();
                let mut data = cloned_state.deref().clone();
                data.server = server;
                cloned_state.set(data);
            }
        }
    });

    // while more verbose, this works
    //callback that observes the username field and saves changes in the local state
    let username_input = Callback::from({
        let cloned_state = login_data_state.clone();
        move |e: InputEvent| {
            let target: Option<EventTarget> = e.target();
            // in order to use dyn_into, use wasm_bindgen::JsCast;
            let input = target.and_then(|t| t.dyn_into::<HtmlInputElement>().ok());
            if let Some(input) = input {
                let username = input.value();
                let mut data = cloned_state.deref().clone();
                data.username = username;
                cloned_state.set(data);
            }
        }
    });

    //callback that observes the password field and saves changes in the local state
    let password_input = Callback::from({
        let cloned_state = login_data_state.clone();
        move |e: InputEvent| {
            let target: Option<EventTarget> = e.target();
            // in order to use dyn_into, use wasm_bindgen::JsCast;
            let input = target.and_then(|t| t.dyn_into::<HtmlInputElement>().ok());
            if let Some(input) = input {
                let password = input.value();
                let mut data: LoginData = cloned_state.deref().clone();
                data.password = password;
                cloned_state.set(data);
            }
        }
    });
    //callback that observes the password field and saves changes in the local state
    let repeat_password_input = Callback::from({
        let cloned_state = login_data_state.clone();
        move |e: InputEvent| {
            let target: Option<EventTarget> = e.target();
            // in order to use dyn_into, use wasm_bindgen::JsCast;
            let input = target.and_then(|t| t.dyn_into::<HtmlInputElement>().ok());
            if let Some(input) = input {
                let repeat_password = input.value();
                let mut data: LoginData = cloned_state.deref().clone();
                data.repeat_password = repeat_password;
                cloned_state.set(data);
            }
        }
    });

    // event handler for login form submission
    // it reads the local state containing username and password and calls the perform_login function
    let onsubmit = {
        let cloned_state = login_data_state.clone();

        if ls_store.is_signup {
            Callback::from(move |event: SubmitEvent| {
                event.prevent_default();
                let data = cloned_state.deref().clone();
                perform_signup(data);
            })
        } else {
            Callback::from(move |event: SubmitEvent| {
                event.prevent_default();
                let data = cloned_state.deref().clone();
                perform_login(data);
            })
        }
    };

    html! {
    <div class="flex min-h-full items-center justify-center px-4 py-12 sm:px-6 lg:px-8">
        <div class="w-full max-w-sm space-y-10">
            <div>
                <div class="text-center">
                    <ApplicationLogo/>
                </div>
                <h2 class="mt-10 text-center text-2xl font-bold leading-9 tracking-tight text-gray-900">if ls_store.is_signup{{"Register a new account"}} else {{"Sign in to your account"}}</h2>
            </div>

            if ls_store.is_signup{
                <p class="text-center text-sm leading-6 text-gray-500">
                    {"Already have an account? "}
                    <button onclick={toggle_is_signup} class="font-semibold text-indigo-600 hover:text-indigo-500">{" go to login"}</button>
                </p>
            } else {
                <p class="text-center text-sm leading-6 text-gray-500">
                    {"Need a new account? "}
                    <button onclick={toggle_is_signup}  class="font-semibold text-indigo-600 hover:text-indigo-500">{"go to signup"}</button>
                </p>
            }

            <form class="space-y-6" {onsubmit}>
                <div class="relative -space-y-px rounded-md shadow-sm">
                    <div class="pointer-events-none absolute inset-0 z-10 rounded-md ring-1 ring-inset ring-gray-300"></div>
                    <div>
                        <label for="server" class="sr-only">{"Server"}</label>
                        <input oninput={server_input} value={login_data_state.server.clone()} id="server" name="server" type="url" required=true class="relative block w-full rounded-t-md border-0 py-1.5 text-gray-900 ring-1 ring-inset ring-gray-100 placeholder:text-gray-400 focus:z-10 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6" placeholder="Server"/>
                    </div>

                    <div>
                        <label for="username" class="sr-only">{"Username"}</label>
                        <input oninput={username_input} value={login_data_state.username.clone()} id="username" name="username" type="text" autocomplete="text" required=true class="relative block w-full  border-0 py-1.5 text-gray-900 ring-1 ring-inset ring-gray-100 placeholder:text-gray-400 focus:z-10 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6" placeholder="Username"/>
                    </div>

                    <div>
                        <label for="password" class="sr-only">{"Password"}</label>
                        <input oninput={password_input} id="password" name="password" type="password" autocomplete="current-password" required=true class="relative block w-full rounded-b-md border-0 py-1.5 text-gray-900 ring-1 ring-inset ring-gray-100 placeholder:text-gray-400 focus:z-10 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6" placeholder="Password"/>
                    </div>
                    if ls_store.is_signup{
                        <div>
                            <label for="repeat_password" class="sr-only">{"Repeat Password"}</label>
                            <input oninput={repeat_password_input} id="repeat_password" name="repeat_password" type="password" autocomplete="current-password" required=true class="relative block w-full rounded-b-md border-0 py-1.5 text-gray-900 ring-1 ring-inset ring-gray-100 placeholder:text-gray-400 focus:z-10 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6" placeholder="Repeat Password"/>
                        </div>
                    }
                </div>

                <div>
                    <button type="submit" class="btn btn-primary">if ls_store.is_signup{{"Register"}} else {{"Sign in"}}</button>
                </div>
                <div>
                    <p>{"server: "} {&login_data_state.server}</p>
                    <p>{"username: "} {&login_data_state.username}</p>
                    <p>{"password: "} {&login_data_state.password.len()}</p>
                    <p>{"repeat password: "} {&login_data_state.repeat_password.len()}</p>


                </div>
            </form>
            <div>

            {
                match &ls_store.error_message {
                    Some(e) => {
                        html! {
                            <>
                            <p class="p-2 rounded bg-red-200 text-red-600">
                                {e}
                            </p>
                            </>
                        }
                    },
                    None => {
                        html! {
                            <>
                            </>
                        }
                    }
                }
            }
            </div>
        </div>

    </div>

    }
}
