use yew::prelude::*;

use crate::components::delete_own_useraccount_button::DeleteOwnUseraccountButton;

#[derive(PartialEq, Properties)]
pub struct SettingsPageProps {}

#[function_component]
pub fn SettingsPage(props: &SettingsPageProps) -> Html {
    let SettingsPageProps {} = props;
    html! {
        <div class="contents">
            <h2 class="font-bold text-xl">{"Settings"}</h2>
            <div>
            <DeleteOwnUseraccountButton/>
            </div>
        </div>
    }
}
