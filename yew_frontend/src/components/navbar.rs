use yew::prelude::*;

use crate::components::{application_logo::ApplicationLogo, logout_button::LogoutButton};

#[derive(PartialEq, Properties)]
pub struct NavbarProps {}

#[function_component]
pub fn Navbar(props: &NavbarProps) -> Html {
    let NavbarProps {} = props;

    html! {
        <nav class="sticky top-0 bg-gray-100 p-4 text-indigo-800 flex flex-row gap-2 justify-between items-center shadow">
            <a href="/" >
                <ApplicationLogo/>
            </a>


            <div class="flex flex-row gap-2 flex-wrap items-center">
                <a class="btn-icon btn-secondary" href="/settings">{"Settings"}</a>
                {"routes here..."}
            </div>
            <LogoutButton/>

        </nav>
    }
}
