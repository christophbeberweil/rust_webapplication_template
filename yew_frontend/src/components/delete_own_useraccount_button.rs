use yew::prelude::*;

use crate::methods::delete_own_useraccount::delete_own_user_account;

#[derive(PartialEq, Properties)]
pub struct DeleteOwnUseraccountButtonProps {}

#[function_component]
pub fn DeleteOwnUseraccountButton(props: &DeleteOwnUseraccountButtonProps) -> Html {
    let DeleteOwnUseraccountButtonProps {} = props;

    let onclick = Callback::from(|_: MouseEvent| delete_own_user_account());
    //let onclick = Callback::from(|_| perform_logout());

    html! {
        <button class="btn btn-danger" {onclick}>{"Delete my user account and all my data (no confirmation)"}</button>
    }
}
