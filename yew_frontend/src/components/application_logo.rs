use yew::prelude::*;

#[derive(PartialEq, Properties)]
pub struct ApplicationLogoProps {}

#[function_component]
pub fn ApplicationLogo(props: &ApplicationLogoProps) -> Html {
    let ApplicationLogoProps {} = props;
    html! {
        <div>{"🚀"}</div>
    }
}
