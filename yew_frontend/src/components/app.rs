use crate::{
    components::{login_page::LoginPage, navbar::Navbar, store_debug::StoreDebug},
    store::RootStore,
};
use yew::prelude::*;
use yew_router::prelude::*;
use yewdux::prelude::*;

use super::{home_page::HomePage, settings_page::SettingsPage};

#[derive(Clone, Routable, PartialEq)]
enum Route {
    #[at("/")]
    Home,
    #[at("/settings")]
    Settings,
    #[not_found]
    #[at("/404")]
    NotFound,
}

fn switch(routes: Route) -> Html {
    match routes {
        Route::Home => html! {  <HomePage/> },
        Route::Settings => html! { <SettingsPage/> },
        Route::NotFound => html! { <p>{ "Nothing to see here... 🤷‍♂️" }</p> },
    }
}

#[derive(PartialEq, Properties, Default)]
pub struct AppProps {}

#[function_component]
pub fn App(props: &AppProps) -> Html {
    let AppProps {} = props;

    let (root_store, _root_store_dispatch) = use_store::<RootStore>();
    //let root_store = dispatch::get::<RootStore>();
    html! {
        <div class="h-screen ">
            <div class="flex flex-col gap-4 relative mb-44">
                    if root_store.token.is_some(){
                        // if we are logged in, show the router and its contents
                        <Navbar/>

                            <BrowserRouter>
                                <div class="p-4 flex flex-col gap-4">
                                    <Switch<Route> render={switch} /> // <- must be child of <BrowserRouter>
                                </div>
                            </BrowserRouter>

                    } else {
                        <LoginPage/>
                    }

            </div>
            <div class="absolute bottom-0 left-0 right-0">
                <StoreDebug/>
            </div>
        </div>
    }
}
