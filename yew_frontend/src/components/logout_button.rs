use yew::prelude::*;

use crate::methods::logout::perform_logout;

#[derive(PartialEq, Properties)]
pub struct LogoutButtonProps {}

#[function_component]
pub fn LogoutButton(props: &LogoutButtonProps) -> Html {
    let LogoutButtonProps {} = props;

    let onclick = Callback::from(|_| perform_logout());

    html! {
        <button class="btn btn-secondary" {onclick} >{"Logout"}</button>
    }
}
