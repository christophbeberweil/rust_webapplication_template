use yew::prelude::*;

use yewdux::prelude::*;

use crate::store::{LoginSignupStore, RootStore};

#[derive(PartialEq, Properties)]
pub struct StoreDebugProps {}

#[function_component]
pub fn StoreDebug(props: &StoreDebugProps) -> Html {
    let StoreDebugProps {} = props;

    let (root_store, _root_store_dispatch) = use_store::<RootStore>();
    let (ls_store, _ls_store_dispatch) = use_store::<LoginSignupStore>();

    html! {
        <div class="bg-gray-200 text-sm p-4 overflow-scroll">
            <p class="font-bold">{"Root store content:"}</p>
            <p>{format!("{root_store:?}")}</p>
            <p class="font-bold">{"LoginSignup store content:"}</p>
            <p>{format!("{ls_store:?}")}</p>
        </div>
    }
}
