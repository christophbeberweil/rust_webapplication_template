use serde::{Deserialize, Serialize};
use yewdux::prelude::*;

#[derive(Debug, Default, Clone, PartialEq, Eq, Store, Serialize, Deserialize)]
#[store(storage = "local", storage_tab_sync)] // storage can also be "session"
pub struct RootStore {
    pub token: Option<String>,
    pub backend_url: Option<String>,
    pub username: Option<String>,
}



#[derive(Debug, Default, Clone, PartialEq, Eq, Store, Serialize, Deserialize)]
#[store(storage = "local", storage_tab_sync)] // storage can also be "session"
pub struct LoginSignupStore{
    pub is_signup: bool,
    pub error_message: Option<String>
}