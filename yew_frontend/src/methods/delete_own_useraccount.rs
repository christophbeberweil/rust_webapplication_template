use gloo_console::log;
use reqwasm::http::{Method, Request};
use yewdux::prelude::*;

use crate::store::RootStore;

use super::logout::perform_logout;

pub fn delete_own_user_account() {
    let root_store_dispatch = Dispatch::<RootStore>::global();

    let root_store = root_store_dispatch.get();

    let Some(backend_url) = root_store.backend_url.clone() else {
        perform_logout();
        log!("Could not delete user account: no backend url");
        return ();
    };

    let Some(token) = root_store.token.clone() else {
        perform_logout();
        log!("Could not delete user account: no token");
        return ();
    };

    wasm_bindgen_futures::spawn_local(async move {
        let endpoint = format!("{}/api/delte_own_useraccount", backend_url);
        let request = Request::new(&endpoint)
            .method(Method::POST)
            .header("Content-Type", "application/json")
            .header("Authorization", format!("Bearer {}", token).as_str())
            .send()
            .await;

        match request {
            Ok(response) => {
                if response.status() == 200 {
                    perform_logout();
                    log!("Your useraccount was deleted.");
                } else {
                    log!("Could not delete user account: ", response.status())
                }
            }
            Err(e) => {
                log!(
                    "Could not send the request to delete the user account: ",
                    e.to_string()
                )
            }
        };
    });
}
