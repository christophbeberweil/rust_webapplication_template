use crate::{
    components::login_page::LoginData,
    store::{LoginSignupStore, RootStore},
};
use gloo_console::debug;
use reqwasm::http::{Method, Request};
use serde::{Deserialize, Serialize};
use serde_json::json;
use uuid::Uuid;
use yewdux::prelude::*;

#[derive(Serialize, Deserialize, Debug)]
pub struct SignupResponse {
    pub id: Uuid,
    pub username: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct SignupErrorResponse {
    pub errors: Vec<String>,
}

/// function that spawns a local future to perform the api request asynchronously
pub fn perform_signup(login_data: LoginData) {
    let root_store_dispatch = Dispatch::<RootStore>::global();

    let ls_store_dispatch = Dispatch::<LoginSignupStore>::global();

    match Ok::<String, serde_json::Error>(json!(login_data).to_string()) {
        Ok(login_data_in_js) => wasm_bindgen_futures::spawn_local(async move {
            let login_endpoint = format!("{}/api/signup", &login_data.server);

            if login_data.password != login_data.repeat_password {
                ls_store_dispatch.reduce_mut(|ls_store| {
                    ls_store.error_message = Some("The passwords do not match!".to_owned());
                });
                return;
            }

            let fetched_result = Request::new(&login_endpoint)
                .method(Method::POST)
                .body(login_data_in_js)
                .header("Content-Type", "application/json")
                .send()
                .await;

            match fetched_result {
                Ok(response) => {
                    let response_text: String = match response.text().await {
                        Ok(t) => t,
                        Err(_) => {
                            ls_store_dispatch.reduce_mut(|ls_store| {
                                ls_store.error_message = Some(
                                    "Could not parse the response from the server: ".to_string(),
                                )
                            });
                            return;
                        }
                    };

                    let parsed_signup_unsuccessful_response =
                        serde_json::from_str::<SignupErrorResponse>(&response_text);
                    let parsed_signup_successful_response =
                        serde_json::from_str::<SignupResponse>(&response_text);

                    // check if signup failed
                    match parsed_signup_unsuccessful_response {
                        Ok(signup_error_response) => {
                            ls_store_dispatch.reduce_mut(|ls_store| {
                                ls_store.error_message =
                                    Some(signup_error_response.errors.join(", "));
                            });
                            return;
                        }
                        Err(e) => {
                            debug!(e.to_string());
                            ls_store_dispatch.reduce_mut(|ls_store| {
                                ls_store.error_message =
                                    Some("Could not parse server response".to_string())
                            });
                        }
                    };

                    // check if signup worked
                    match parsed_signup_successful_response {
                        Ok(signup_response) => {
                            root_store_dispatch.reduce_mut(|root_store| {
                                root_store.username = Some(signup_response.username);
                            });
                            ls_store_dispatch.reduce_mut(|ls_store| {
                                ls_store.error_message = None;
                                ls_store.is_signup = false;
                            });
                        }
                        Err(e) => {
                            debug!(e.to_string());
                            ls_store_dispatch.reduce_mut(|ls_store| {
                                ls_store.error_message =
                                    Some("Could not parse server response".to_string())
                            });
                        }
                    };
                }
                Err(e) => {
                    ls_store_dispatch
                        .reduce_mut(|ls_store| ls_store.error_message = Some(e.to_string()));
                }
            }
        }),
        Err(_e) => {}
    };
}
