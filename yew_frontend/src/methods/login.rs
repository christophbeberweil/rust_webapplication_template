use reqwasm::http::{Method, Request};
use serde_json::json;
use yewdux::prelude::*;

use crate::{
    components::login_page::LoginData,
    store::{LoginSignupStore, RootStore},
};
use gloo_console::debug;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct LoginResponse {
    pub token: String,
}
#[derive(Serialize, Deserialize, Debug)]
pub struct LoginErrorResponse {
    pub error: String,
    pub status: String,
}

/// function that spawns a local future to perform the api request asynchronously
pub fn perform_login(login_data: LoginData) {
    let root_store_dispatch = Dispatch::<RootStore>::global();

    let ls_store_dispatch = Dispatch::<LoginSignupStore>::global();

    match Ok::<String, serde_json::Error>(json!(login_data).to_string()) {
        Ok(login_data_in_js) => wasm_bindgen_futures::spawn_local(async move {
            let login_endpoint = format!("{}/api/login", &login_data.server);
            let fetched_result = Request::new(&login_endpoint)
                .method(Method::POST)
                .body(login_data_in_js)
                .header("Content-Type", "application/json")
                .send()
                .await;

            match fetched_result {
                Ok(response) => {
                    let response_text: String = match response.text().await {
                        Ok(t) => t,
                        Err(_) => {
                            ls_store_dispatch.reduce_mut(|ls_store| {
                                ls_store.error_message = Some(
                                    "Could not parse the response from the server: ".to_string(),
                                )
                            });
                            return;
                        }
                    };

                    let parsed_login_unsuccessful_response =
                        serde_json::from_str::<LoginErrorResponse>(&response_text);
                    let parsed_login_successful_response =
                        serde_json::from_str::<LoginResponse>(&response_text);

                    // check if login failed
                    match parsed_login_unsuccessful_response {
                        Ok(signup_error_response) => {
                            ls_store_dispatch.reduce_mut(|ls_store| {
                                ls_store.error_message = Some(signup_error_response.error);
                            });
                            return;
                        }
                        Err(e) => {
                            debug!(e.to_string());
                            ls_store_dispatch.reduce_mut(|ls_store| {
                                ls_store.error_message =
                                    Some("Could not parse server response".to_string())
                            });
                        }
                    };

                    // check if login worked
                    match parsed_login_successful_response {
                        Ok(login_response) => {
                            ls_store_dispatch.reduce_mut(|ls_store| {
                                ls_store.error_message = None;
                                ls_store.is_signup = false;
                            });
                            // write to store
                            root_store_dispatch.reduce_mut(|root_store| {
                                root_store.token = Some(login_response.token);
                                root_store.backend_url = Some(login_data.server);
                                root_store.username = Some(login_data.username);
                            });

                            // refersh everything upon successful login here

                            // todo
                        }
                        Err(e) => {
                            debug!(e.to_string());
                            ls_store_dispatch.reduce_mut(|ls_store| {
                                ls_store.error_message =
                                    Some("Could not parse server response".to_string())
                            });
                        }
                    };
                }
                Err(e) => {
                    ls_store_dispatch
                        .reduce_mut(|ls_store| ls_store.error_message = Some(e.to_string()));
                }
            }
        }),
        Err(_e) => {}
    };
}
