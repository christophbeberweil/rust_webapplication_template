use yewdux::prelude::*;

use crate::store::RootStore;

pub fn perform_logout() {
    let root_store_dispatch = Dispatch::<RootStore>::global();
    root_store_dispatch.reduce_mut(|root_store| root_store.token = None);
}
